/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.captain.oxgameui;

import java.io.Serializable;

/**
 *
 * @author captain
 */
public class Table implements Serializable{

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private Player winner;
    private boolean finish = false;
    private int lastCol;
    private int lastRow;
    int round = 0;
    public Table(Player x, Player o) {
        playerX = x;
        playerO = o;
        currentPlayer = x;
    }

    public void showTable() {
        System.out.println(" 1 2 3");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col] + " ");
            }
            System.out.println("");
        }
    }
    public char getRowCol(int row,int col){
        return table[row][col];
    }
    public boolean setRowCol(int row, int col) {
        if(isFinish())return false;
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            round++;
            this.lastCol = lastCol;
            this.lastRow = lastRow;          
            checkWin();   
            return true;
        }
        return false;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void switchPlayer() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;
        } else {
            currentPlayer = playerX;
        }
    }

    void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][0] == table[row][1] && table[row][1] == table[row][2]
                    && table[row][0] != '-') {

                finish = true;
                winner = currentPlayer;
                setStatWinLose();
            }
        }
    }

    private void setStatWinLose() {
        if (currentPlayer == playerO) {
            playerO.win();
            playerX.lose();

        } else {
            playerO.lose();
            playerX.win();

        }
    }

    void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[0][col] == table[1][col] && table[1][col] == table[2][col]
                    && table[0][col] != '-') {

                finish = true;
                winner = currentPlayer;
                setStatWinLose();
            }
        }
    }

    void checkDiagonal() {
        for (int col = 0; col < 3; col++) {
            if (table[col][col] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    void checkDiagonal2() {
        int R = 2;
        for (int row = 0; row < 3; row++, R--) {
            if (table[row][R] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }
    public void checkDraw(){
        if(round==9&&finish==false){
            playerX.draw();
            playerO.draw();
            finish = true;
        }
    }

    public void checkWin() {
        checkRow();
        checkCol();
        checkDiagonal();
        checkDiagonal2();
        checkDraw();
    }

    public boolean isFinish() {
        return finish;
    }

    public Player getWinner() {
        return winner;
    }

}
